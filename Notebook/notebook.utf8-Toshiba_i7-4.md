
<!-- rnb-text-begin -->

---
title: "System Administration"
author: Mehmet Can Boysan
output:
  html_document: default
  html_notebook: default
---

**Legend:**
<div style="background-color:#ffddcc; padding:1px;">
Execute
</div>
<div style="background-color:#ddffcc; padding:1px;">
 Check
</div>

## Root Access

<div style="background-color:#ffddcc; padding:20px 20px;">

* Boot into grub, select single user but do not press enter.
* Press e to go into edit mode.
* Scroll down to the kernel line you will boot from, it starts with "linux /boot/vmlinuz-..."
* Scroll to the end of that line and press space key once and type init=/bin/bash
* Press Ctrl X to boot

```
# Remount / as Read/Write 
mount -rw -o remount /

# check if admin exists
cut -d: -f1 /etc/passwd | grep admin

# if not 
adduser admin
```

* nano **/etc/sudoers** or ```visudo``` to update sudoers and add line:
```
admin   ALL=NOPASSWD: ALL
```

* reboot and log in with *admin* and use
```
sudo su -
```

Now you are root!

</div>

## DHCP Network

<div style="background-color:#ffddcc; padding:20px 20px;">

* If the physical machine has access to internet, set VirtualBox Network adapter to **NAT**.
* In VM, backup **/etc/resolv.conf**
* In the **/etc/network/interfaces**
```
# set this for eth0 adapter:
iface eth0 inet dhcp
```
* reboot

</div>

## Static Network

<div style="background-color:#ffddcc; padding:20px 20px;">
```
# turn off eth0
ifdown eth0

# eth0 doesn't have an ip
ifconfig
```

* static ip **/etc/network/interfaces**

Add this:

```
iface eth0 inet static
    address 192.168.10.166
    netmask 255.255.255.0
    gateway 192.168.10.1
```

* **/etc/resolv.conf**

Only one line should exist:

```
# for local resolver (PREFERRED - after completing bind9 settings):
nameserver 127.0.0.1

# for resolving through gateway:
nameserver 192.168.10.1

# for resolving through google:
nameserver 8.8.8.8
```

</div>

<div style="background-color:#ddffcc;  padding:20px 20px;">
```
# ip address should change
ip addr l

# ping google dns
ping 8.8.8.8

# should work
ping www.google.com
```
</div>

<div style="background-color:#ffddcc; padding:20px 20px;">

* **/etc/apt/sources.list**

```
deb http://ftp.ee.debian.org/debian jessie main 
deb-src http://ftp.ee.debian.org/debian jessie main

deb http://ftp.ee.debian.org/debian jessie-updates main
deb-src http://ftp.ee.debian.org/debian jessie-updates main

deb http://security.debian.org/ jessie/updates main
deb-src http://security.debian.org/ jessie/updates main
```

```
apt-get update
apt-get upgrade

apt-get install iptables
```

Follow **```IpTables and Ports```** section.

Disable IPv6 (may not be necessary)

* **/etc/sysctl.conf**
Add the following lines:
```
net.ipv6.conf.default.disable_ipv6 = 1
net.ipv6.conf.all.disable_ipv6 = 1
```

Run aftwerwards:
```
sysctl -p /etc/sysctl.conf
```

</div>


## SSH Server

<div style="background-color:#ffddcc; padding:20px 20px;">

* disable firewall first.
* important points **/etc/ssh/sshd_config**

```
RSAAuthentication yes
PubkeyAuthentication yes
AuthorizedKeysFile     %h/.ssh/authorized_keys

ChallengeResponseAuthentication no

# should be set to no later
PasswordAuthentication yes
PermitEmptyPasswords no

UsePAM yes

# comment out:
#KerberosAuthentication no
#GSSAPIAuthentication n
```

* check permissions and owners of **${HOME}/.ssh**:

![](https://owncloud.ut.ee/owncloud/index.php/s/uO7auATsur45n1H/download?path=%2F&files=home_user_ssh.png)

```
chown -R user:user /home/admin/.ssh
chmod 700 /home/admin/.ssh
chmod 600 /home/admin/.ssh/authorized_keys
```
</div>

<div style="background-color:#ddffcc; padding:1px;">

Follow lab tutorials to check if ssh works.

</div>


## DNS Resolver

<div style="background-color:#ffddcc; padding:20px 20px;">

* check **/etc/hostname**
```
host59.mehmet91.est
```

* check **/etc/hosts**
```
127.0.0.1       localhost
::1             localhost ip6-localhost ip6-loopback
ff02::1         ip6-allnodes
ff02::2         ip6-allrouters
```

</br>

**Bind 9 DNS Server:**

```
apt-get install bind9 bind9utils bind9-doc dnsutils
```
Bind main config files in **/etc/bind** folder:

* ```named.conf``` - main file
* ```named.conf.local``` - local definitions
* ```named.conf.options``` - options file
* ```named.conf.logging``` - logging options


Bind log files and Permissions **/var/log/bind**:

![](https://owncloud.ut.ee/owncloud/index.php/s/uO7auATsur45n1H/download?path=%2F&files=bind9_log_files.png)

```
chown -R bind:bind /var/log/bind9
chmod -R 640 /var/log/bind9
```

**/etc/bind** directory permissions:

![](https://owncloud.ut.ee/owncloud/index.php/s/uO7auATsur45n1H/download?path=%2F&files=etc_binddir.png)

Create necessary files (log conf file etc.) in **/etc/bind** and arrange permissions:

![](https://owncloud.ut.ee/owncloud/index.php/s/uO7auATsur45n1H/download?path=%2F&files=etc_bind.png)

```
chown root:bind /etc/bind/named.conf*
chmod 640 /etc/bind/named.conf*

chown bind:bind /etc/bind/named.conf.logging
chmod 640 /etc/bind/named.conf.logging
```

* **check contents of main config files.**

* restart bind service:

```
service bind9 stop
service bind9 start
service bind9 restart
```

* check **/etc/resolv.conf**

* start bind service at system start if needed.

```
update-rc.d bind9 enable
```

</div>

<div style="background-color:#ddffcc;  padding:20px 20px;">

```
# check config of bind9:
named-checkconf
```

```
service bind9 status
systemctl status bind9.service -l
journalctl -xn
```

If everything works fine and no error is reported, you may check how your local resolver is performing:

```
dig @127.0.0.1 courses.cs.ut.ee
dig @127.0.0.1 www.google.com
```
It should answer with list of IP addresse for courses.cs.ut.ee

```
# should work
ping www.google.com
```

</div>

## DNS Local Resolver

<div style="background-color:#ffddcc; padding:20px 20px;">

Replace bind configuration files with preconfigured ones.

Ip to be used: ```192.168.10.166```

* check **/etc/bind/named.conf.options** for ip
* check **/etc/bind/named.conf.local** for domain-name, gateway, zone-ip declerations and file names

zone files:

* **don't forget to update serial number**
* check **/etc/bind/zones/10.168.192.in-addr.arpa.zone** for domain-name and ip
* check **/etc/bind/zones/mehmet91.est.zone** for host-name, domain-name, ip

</div>

<div style="background-color:#ddffcc; padding:20px 20px;">

```
named-checkzone ''10.168.192.in-addr.arpa'' /etc/bind/zones/10.168.192.in-addr.arpa.zone
named-checkzone ''mehmet91.est'' /etc/bind/zones/mehmet91.est.zone
named-checkconf

dig @127.0.0.1 <your_domain>
dig @127.0.0.1 ns1.<your_domain>
dig @127.0.0.1 <your_host>.<your_domain>
dig @127.0.0.1 <your_domain> NS


### REVERSE MAPPING CHECK
# should give FQDN hostname (for checking reverse mapping)
host 192.168.10.166
host <teacher ip>

dig @192.168.10.1 mail.teacher.est

### DNS CHECK
nslookup www.ut.ee
nslookup yourhost.yourdomain.est
nslookup host59.mehmet91.est

# should give two results: 127.0.0.1:53 and 192.168.10.1:53
netstat -upln | grep named

### WORLD RESOLVER CHECK
# should give an IP address of the www.ut.ee
dig @127.0.0.1 www.ut.ee
# should give REFUSED
dig @192.168.10.166 www.ut.ee
```

</div>

## Mail Server

<div style="background-color:#ddffcc; padding:20px 20px;">

* First check:
```
dig @192.168.10.1 mail.mehmet91.est
```

</div>

<div style="background-color:#ffddcc; padding:20px 20px;">

* Disable firewall first!

```
apt-get update
apt-get install ntp
```

* **/etc/ntp.conf**, make sure this line exists:
```
server ntp.ut.ee
```
```
service ntp restart
service ntp status
```
<div style="background-color:#ddffcc; padding:20px 20px;">
```
# check date:
date
```

* Install required packages:
```
apt-get install postfix dovecot-imapd spamassassin alpine procmail
```

* Reconfigure postfix if necessary **(as a last resort)**, see dpkg-reconfigure command


* Generate ssl certificates and keys for **postfix** and **dovecot** if they don't exist

```
openssl req -new -x509 -days 3650 -nodes -out /etc/ssl/certs/dovecot.pem -keyout /etc/ssl/private/dovecot.key
openssl req -new -x509 -days 3650 -nodes -out /etc/ssl/certs/postfix.pem -keyout /etc/ssl/private/postfix.key
```

* Check certs and keys permissions:

![](https://owncloud.ut.ee/owncloud/index.php/s/uO7auATsur45n1H/download?path=%2F&files=etc_ssl_private.png)

```
chown root:ssl-cert /etc/ssl/private/postfix.key
chown root:ssl-cert /etc/ssl/private/dovecot.key
chmod u=r,g=r,o= /etc/ssl/private/postfix.key
chmod u=r,g=r,o= /etc/ssl/private/dovecot.key
```

![](https://owncloud.ut.ee/owncloud/index.php/s/uO7auATsur45n1H/download?path=%2F&files=etc_ssl_certs.png)

```
chown root:root /etc/ssl/certs/postfix.pem
chown root:root /etc/ssl/certs/dovecot.pem
chmod u=rw,g=r,o=r /etc/ssl/certs/postfix.pem
chmod u=rw,g=r,o=r /etc/ssl/certs/dovecot.pem
```

<div style="background-color:#ddffcc; padding:20px 20px;">
Those commands should show the same value:
```
openssl x509 -noout -modulus -in /etc/ssl/certs/postfix.pem | openssl md5
openssl rsa -noout -modulus -in /etc/ssl/private/postfix.key | openssl md5

openssl x509 -noout -modulus -in /etc/ssl/certs/dovecot.pem | openssl md5
openssl rsa -noout -modulus -in /etc/ssl/private/dovecot.key | openssl md5
```
</div>

### Postfix

```
# user altered variables
postconf -n

# default values
postconf -d
```

Postfix log files:

* **/var/log/mail.log**
* **/var/log/messages**

Postfix configuration files:

* **/etc/postfix/main.cf**
* **/etc/postfix/master.cf**
* **/etc/mailname**

Replace postfix configuration files listed above, and arrange parameters in **/etc/postfix/main.cf**:

* myhostname = host59.mehmet91.est
* smtpd_tls_cert_file=/etc/ssl/certs/postfix.pem
* smtpd_tls_key_file=/etc/ssl/private/postfix.key

Edit **/etc/mailname**

Edit **/etc/canonical** and compile it with:
```
postmap /etc/postfix/canonical

# check if /etc/postfix/canonical.db exists
```

Make postfix start at system start
```
update-rc.d postfix enable
```

Check **/etc/aliases**
If /etc/aliases are replaced run:
```
newaliases
postfix reload
```

<div style="background-color:#ddffcc; padding:20px 20px;">
* Change your user to mail testing user (mailuser).
* Run alpine mail client. Send an email to your main user account.
* Return to your previous session (and if not there) log in as user and check your mailbox.
* If sending e-mails between the local users works, try to exchange e-mails with other lab systems (ask for their e-mail addresses).
* Alternatively you may exchange the email with lab assistant, sending mails to teacher@teacher.est.
* The correct configured postfix shall not put the FDQN of your host into from field, only the domain part.
</div>

### Alpine specific details:

* replace **/etc/pine.conf**
* replace **user-domain** parameter in line 23

### Dovecot

```
# user altered variables
doveconf -n

# default values
doveconf -d
```

#### Logging

* Replace **/etc/dovecot/conf.d/10-logging.conf** file
* Make sure there is a file **/var/log/dovecot/dovecot.log**
* Check permissions of log folder **/var/log/dovecot** and**/var/log/dovecot/dovecot.log** file

![](https://owncloud.ut.ee/owncloud/index.php/s/uO7auATsur45n1H/download?path=%2F&files=var_log_dovecot.png)

```
chmod 4755 /var/log/dovecot
chown root:root /var/log/dovecot/dovecot.log
chmod u=rw,g=,o= /var/log/dovecot/dovecot.log
```

#### SSL

* Replace **/etc/dovecot/conf.d/10-master.conf**
* Replace **/etc/dovecot/conf.d/10-ssl.conf**
* Replace **/etc/dovecot/conf.d/10-auth.conf**
* Replace **/etc/dovecot/conf.d/10-mail.conf**

* Check **/var/mail** permissions:

![](https://owncloud.ut.ee/owncloud/index.php/s/uO7auATsur45n1H/download?path=%2F&files=var_mail.png)

```
chmod 2775 /var/mail
chown root:mail /var/mail
chgrp -R mail /var/mail
```

#### Dovecot Finalize

```
update-rc.d dovecot enable
```

### SPAM Assassin

```
apt-get install procmail
```

* Replace **/home/mailuser/.procmailrc**

```
update-rc.d spamassassin enable
```

### Finalize

```
service postfix restart
service dovecot restart
service spamassassin restart
```
</div>

<div style="background-color:#ddffcc; padding:20px 20px;">

Check mails!

</div>



## Apache

<div style="background-color:#ffddcc; padding:20px 20px;">

* Check canonical names in **/etc/bind/zones/yourdomain.est.zone**

<div style="background-color:#ddffcc; padding:20px 20px;">
These should work:
```
nslookup webmail.yourdomain.est
nslookup www.yourdomain.est
```
</div>

```
apt-get install apache2
apt-get install lynx
```

Disable all virtual hosts
```
cd /etc/apache2/sites-available

a2dissite ./*
```

Disable these mods and confs:
```
a2dismod userdir
a2dismod status
a2disconf phpmyadmin
```

```
service apache2 restart
```

Replace all files in **/etc/apache2/sites-available** and replace domain

* Disable RewriteEngine in virtual hosts for testing

Replace **/etc/apache2/mods-available/status.conf**

Check these directories and copy necessary files:

* **/var/www**
* **/var/www/html**
* **/var/www/vhosts**
* **/var/www/vhosts/webmail**
* **/var/www/vhosts/mehmet91.est**

### Usermod

Create **/home/user/public_html/index.html** file:
Check **/home/user/public_html** permissions

![](https://owncloud.ut.ee/owncloud/index.php/s/uO7auATsur45n1H/download?path=%2F&files=home_user_publichtml.png)

```
chmod 701 /home/user

chown -R user:user /home/user/public_html
chmod 705 /home/user/public_html
```
<div style="background-color:#ddffcc; padding:20px 20px;">
Check: http://www.mehmet91.est/~user/
</div>

### PHP5

```
apt-get install php5
service apache2 restart
```

If restart fails, disable the MPM module on Apache conf file or try to upgrade PHP module to a thread-safe binary.


### HTTP Auth

* **Create tunnel from Host to VM**

* Create **/etc/htpasswd** folder and run:

```
# creating the .htpasswd file with a new user
htpasswd -c /etc/htpasswd/.htpasswd user

# create new user
htpasswd /etc/htpasswd/.htpasswd UserNameHere
```

<div style="background-color:#ddffcc; padding:20px 20px;">
Check: http://sample1.mehmet91.est/sample1/sample1.html
</div>

### PAM

```
apt-get install libapache2-mod-authnz-external pwauth
```
```
a2enmod authnz_external
```

<div style="background-color:#ddffcc; padding:20px 20px;">
Check: http://sample1.mehmet91.est/sample1/sample1.html
</div>

### MySQL

```
apt-get install mysql-server mysql-client
apt-get install php5-mysql
```
```
dpkg-reconfigure mysql-server-5.5
```

Reconfigure:

* Insert and remember the password for root (will be asked during installation)
* By default it should start the mysql service and enable it on startup, but it is a good practice to check it.

```
mysql -p -u root

GRANT SELECT ON *.* TO 'tester'@'localhost';
```

<div style="background-color:#ddffcc; padding:20px 20px;">
```
mysql -u tester

# check databases
mysql> SHOW DATABASES;
```
</div>

### PhpMyAdmin

```
apt-get install phpmyadmin
```
```
dpkg-reconfigure phpmyadmin
```

Reconfigure:

* Choose apache server (need to press space to select)
* Choose yes to configure database with dbconfig-common
* Insert your root password for Password of the database's administrative user:
* Insert a new generic password for the phpmyadmin user

```
a2disconf phpmyadmin.conf
```

```
apache2ctl configtest
service apache2 restart
```

<div style="background-color:#ddffcc; padding:20px 20px;">
Check: http://www.mehmet91.est/phpmyadmin
</div>


### Roundcube

* Check if **/opt/roundcube** exists, if not make dir

```
tar -xzvf /media/sf_share/roundcubemail-1.1.4-complete.tar.gz -C /opt/roundcube/
cd /opt/roundcube
ln -s /opt/roundcube/roundcubemail-1.1.4/ roundcube
chown www-data roundcubemail-1.1.4
```

```
mysql -p -u root

CREATE DATABASE roundcubemail;
GRANT ALL PRIVILEGES ON roundcubemail.* TO roundcube@localhost IDENTIFIED BY '';
flush privileges;

exit;
```
```
mysql -p -u roundcube roundcubemail < /opt/roundcube/roundcubemail/SQL/mysql.initial.sql
```

<div style="background-color:#ddffcc; padding:20px 20px;">
```
apache2ctl configtest
```
</div>

</div>



## IpTables and Ports

<div style="background-color:#ffddcc; padding:20px 20px;">

### Disable IpTables

```
# backup iptables
iptables-save > /etc/firewall.conf.bak
```

```
iptables -F
iptables -X
iptables -t nat -F
iptables -t nat -X
iptables -t mangle -F
iptables -t mangle -X
iptables -P INPUT ACCEPT
iptables -P OUTPUT ACCEPT
iptables -P FORWARD ACCEPT
```

</div>

</br>

<div style="background-color:#ffddcc; padding:20px 20px;">

### IpTables Setup

```
# SAVE IP TABELS INTO FILE
iptables-save > /etc/firewall.conf

# RESTORE IP TABLES FROM FILE
iptables-restore < /etc/firewall.conf

# RESTORE IP TABLES IN STARTUP
echo "iptables-restore < /etc/firewall.conf" > /etc/network/if-up.d/iptables

# MAKE IT EXECUTABLE
chmod u+x /etc/network/if-up.d/iptables
```
</div>


<div style="background-color:#ddffcc;  padding:20px 20px;">

* **/etc/network/if-up.d/iptables**

```
#!/bin/bash
iptables-restore < /etc/firewall.conf

```

* **/etc/firewall.conf**

```
# Generated by iptables-save v1.4.21 on Thu Mar  2 16:04:37 2017
*filter
:INPUT ACCEPT [0:0]
:FORWARD ACCEPT [0:0]
:OUTPUT ACCEPT [106:8420]
-A INPUT -i lo -j ACCEPT
-A INPUT -d 127.0.0.0/8 ! -i lo -j REJECT --reject-with icmp-port-unreachable
-A INPUT -m state --state RELATED,ESTABLISHED -j ACCEPT
-I INPUT -m state --state NEW -p tcp -m tcp --dport 22 -j ACCEPT
-I INPUT -m state --state NEW -p tcp -m tcp --dport 25 -j ACCEPT
-I INPUT -m state --state NEW -p tcp -m tcp --dport 80 -j ACCEPT
-I INPUT -m state --state NEW -p tcp -m tcp --dport 443 -j ACCEPT
-I INPUT -m state --state NEW -p tcp -m tcp --dport 993 -j ACCEPT
-I INPUT -m state --state NEW -p tcp -m tcp --dport 587 -j ACCEPT
-I INPUT -m state --state NEW -p tcp -m tcp --dport 143 -j ACCEPT
-I INPUT -m state --state NEW -p tcp -m tcp --dport 993 -j ACCEPT
-I INPUT -m state --state NEW -p tcp -m tcp --dport 139 -j ACCEPT
-I INPUT -m state --state NEW -p tcp -m tcp --dport 445 -j ACCEPT
-I INPUT -m state --state NEW -p tcp -m tcp --dport 111 -j ACCEPT
-I INPUT -m state --state NEW -p tcp -m tcp --dport 2049 -j ACCEPT
-I INPUT -m state --state NEW -p udp -m udp --dport 53 -j ACCEPT
-I INPUT -m state --state NEW -p udp -m udp --dport 138 -j ACCEPT
-I INPUT -m state --state NEW -p udp -m udp --dport 137 -j ACCEPT
-I INPUT -m state --state NEW -p udp -m udp --dport 111 -j ACCEPT
-I INPUT -m state --state NEW -p udp -m udp --dport 2049 -j ACCEPT
-A INPUT -p icmp -j ACCEPT
-A INPUT -m limit --limit 50/min -j LOG --log-prefix "Firewall denied 50/m:"
-A INPUT -j DROP
COMMIT
# Completed on Thu Mar  2 16:04:37 2017

```
</div>

## VirtualBox Guest Additions

* **/etc/apt/sources.list**

```
deb http://ftp.ee.debian.org/debian jessie main 
deb-src http://ftp.ee.debian.org/debian jessie main

deb http://ftp.ee.debian.org/debian jessie-updates main
deb-src http://ftp.ee.debian.org/debian jessie-updates main

deb http://security.debian.org/ jessie/updates main
deb-src http://security.debian.org/ jessie/updates main

deb http://ftp.debian.org/debian jessie main contrib
```

```
apt-get update
apt-get install build-essential module-assistant
apt-get install virtualbox-guest-dkms linux-headers-$(uname -r)

reboot now
```

* mount windows share:

```
mkdir /media/share
mount -t vboxsf share /media/share
```

## USEFUL COMMANDS

```
# find all ip addresses that are bound
nmap -sP 192.168.10.0/24

# flush ip
ip addr flush dev eth0

# find default gateway
ip route | grep default
```

```
# show line numbers in nano
nano -c [filename]
```

```
# find value in files with line number
grep -Hni <value> ./*.conf


# find value in files with line number recursively
grep -Rni <value> ./


# find files in a directory
find ./ -iname '*.conf'


# grep things that doesn't contain value
grep -v <value>

# regexp version
grep -vE '#'

# sample usage:
grep -Hni Rewrite ./*.conf | grep -v '#'
```

```
# copy file by preserving mode,ownership,timestamps
cp -p <source> <destination>
```

```
# see uid and gid
id foo

# To assign a new UID to user called foo, enter:
usermod -u 2005 foo

# To assign a new GID to group called foo, enter:
groupmod -g 3000 foo
```

```
dpkg-reconfigure <package-name>
```

<!-- rnb-text-end -->

